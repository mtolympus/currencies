# Laravel Currencies Package

This package provides your application with the currencies you want and current exchange rates between them. It connects to multiple (configurable) 3rd party API's for it's data.

## Installation

### Step 1.

Install the package by running the following command:

```
composer require hermes/currencies
```

### Step 2.

Publish the package's assets (migration, seed and config file) by running the following command:

```
php artisan vendor:publish --provider="Hermes/Currencies/Provider/CurrenciesServiceProvider"
```

### Step 3.

Run the migration by running:

```
php artisan migrate
```

Edit the ```app/Database/seeds/CurrencySeeder.php``` file to your liking and run:

```
php artisan db:seed
```
or what I usually end up doing is
```
php artisan migrate:refresh --seed
```

### Step 4.

Make sure you have a queue worker running in the background and schedule the ```Hermes\Currencies\Jobs\UpdateCurrencyConversionRates``` job at the desired interval.
You can do this by updating your ```app/Console/Kernel.php``` file, see the Laravel Documentation for more details.

```
protected function schedule(Schedule $schedule)
{
    ...

    // Update conversion rates every 15 minutes
    $schedule->job(new UpdateCurrencyConversionRates)->everyFifteenMinutes();
}
```

### Step 5.

Add the following environment variables to your ```.env``` file:

```
FIXER_API_KEY=yourkeyhere
FIXER_PREMIUM=false
```

## Usage

Todo ...