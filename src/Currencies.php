<?php

namespace Hermes\Currencies;

use Meta;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Hermes\Currencies\Models\Currency;

class Currencies
{
    private $cca_api_key;
    private $fixer_api_key;

    public function __construct()
    {
        $this->cca_api_key = env("CCA_KEY");
        $this->fixer_api_key = env("FIXER_KEY");
    }

    /**
     * Get currency by it's name
     * 
     * @param       string                                  The name of the currency we want to retrieve
     * @return      Hermes\Currencies\Models\Currency       The currency we want or false if it does not exist
     */
    public function get(string $currencyName)
    {
        // Find the currency using it's name
        $currency = Currency::where("name", $currencyName)->first();

        // If we found the currency, return it
        if ($currency)
        {
            return $currency;
        }

        // If we did not find it, return false
        return false;
    }

    /**
     * Update conversion rates
     * 
     * @return      void
     */
    public function updateConversionRates()
    {
        // Loop through all available currencies
        foreach (Currency::all() as $currency)
        {
            // Grab the latest exchange rates for this currency compared with the other currencies
            $rates = $this->retrieveExchangeRates($currency);

            // Save the rates on the currency
            $currency->conversion_rates = $rates;
            $currency->save();

            // Update the "last updated" meta
            Meta::set("currency_conversion_rates_last_update", Carbon::now());
        }
    }

    /**
     * Last conversion rate update
     * 
     * @return      String          date
     */
    public function lastConversionRateUpdate()
    {
        return Meta::getValue("currency_conversion_rates_last_update");
    }

    /**
     * Retrieve exchange rates for given currency
     * Uses fallback api's in case of failure on one of the endpoints
     * 
     * @param           App\Models\Currency             The currency we want to receive the exchange rates of
     * @return          array                           Array of currency codes and their corresponding exchange rates
     */
    public function retrieveExchangeRates(Currency $currency)
    {
        // Compose list of target currency codes which we want the exchange rates of
        $targets = [];
        foreach (Currency::all() as $c)
        {
            if ($c->id != $currency->id)
            {
                $targets[] = $c->code;
            }
        }

        // Retrieve the exchange rates from the apis using multiple fallbacks in case of failure from one of the api's
        return $this->retrieveExchangeRatesFromApis($currency, $targets);
    }

    /**
     * Retrieve exchange rates from external api's
     * 
     * @param           App\Models\Currency             The currency we want to convert to x,y,z
     * @param           array                           Array of currency codes we want the exchange rates
     * @return          array                           Array of currency codes and their corresponding exchange rates
     */
    public function retrieveExchangeRatesFromApis(Currency $currency, array $targetCurrencies)
    {
        $fixer_premium = env("FIXER_PREMIUM", false);

        // Use the Fixr.io API as the first option & if that succeeds return those rates, if we fail; continue to fallback procedures
        if ($currency->code == "USD" or $fixer_premium)
        {
            $ratesFromFixr = $this->retrieveExchangeRatesFromFixer($currency, $targetCurrencies);
            if ($ratesFromFixr)
            {
                return $ratesFromFixr;
            }
        }

        // Use the free.currencyconverterapi.com
        $ratesFromCca = $this->retrieveExchangeRatesFromCca($currency, $targetCurrencies);
        if ($ratesFromCca)
        {
            return $ratesFromCca;
        }

        // Return false if the API calls all failed
        return false;
    }

    /**
     * Retrieve exchange rates from Fixr
     * 
     * @param           App\Models\Currency             The currency we want to use as base currency
     * @param           Array                           Array of currency codes we want the exchange rates of
     * @return          json                            Response from Fixr
     */
    public function retrieveExchangeRatesFromFixer(Currency $currency, array $targetCurrencies)
    {
        // Grab the API KEY from the .env file
        $fixer_api_key = env("FIXER_KEY");

        // Create a new guzzle http client
        $client = new Client();

        // Convert array of targets to an comma-seperated string
        $targets = implode(",", $targetCurrencies);

        // Compose API endpoint URI
        $endpoint = "http://data.fixer.io/api/latest?access_key=".$fixer_api_key."&base=".$currency->code."&symbols=".$targets;

        // Make the API call
        $response = $client->get($endpoint);

        // Get body of the response, which is a stream thanks to PSR-7 whatever
        // http://docs.guzzlephp.org/en/latest/psr7.html#body
        $body = $response->getBody();

        // Extract the response from the body and decode it from it's json stringyness
        $response = json_decode($body->getContents());

        // If we successfully received some exchange rate data
        if ($response->success)
        {
            // Return the exchange rates array
            return $response->rates;
        }

        // Return the data from the response
        return false;
    }

    /**
     * Retrieve exchange rates from currencyconverterapi.com
     * 
     * @param           App\Models\Currency             The currency we want to use as base currency
     * @param           Array                           Array of currency codes we want the exchange rates of
     * @param           json                            Response from 
     */
    public function retrieveExchangeRatesFromCca(Currency $currency, array $targetCurrencies)
    {
        // Create a new guzzle http client
        $client = new Client();

        // Save the base uri of the API's endpoint since we need to make multiple calls (for each target currency)
        $baseApiEndpoint = "https://free.currencyconverterapi.com/api/v6/convert?apiKey=".$this->cca_api_key."&compact=ultra&q=";

        // Collect the conversion rates
        $rates = [];

        // Compose the query so we only have to make one request
        $query = "";
        foreach ($targetCurrencies as $targetCurrency)
        {
            if ($query != "") $query .= ",";
            $query .= $currency->code."_".$targetCurrency;
        }

        // Make the API request
        $response = $client->get($baseApiEndpoint.$query);
        $response = json_decode($response->getBody(), true);

        // If we received some conversion rates
        if (count($response) > 0)
        {
            foreach ($response as $currencyCodes => $conversionRate)
            {
                $currencyCodes = explode("_", $currencyCodes);
                $rates[$currencyCodes[1]] = $conversionRate;
            }
        }

        // If we found some exchange rates, return them
        if (count($rates) > 0)
        {
            return $rates;
        }

        // If we failed
        return false;
    }

    /**
     * All currency conversion rates
     * 
     * @return          Illuminate\Support\Collection
     */
    public function allConversionRates()
    {
        // The output array
        $out = [];

        // Grab all currencies
        $currencies = Currency::all();

        // If we found some currencies to process
        if ($currencies->count() > 0)
        {
            // Loop through all of the currencies
            foreach ($currencies as $currency)
            {
                $currencyRates = [
                    "currency" => $currency,
                    "rates" => []
                ];
                
                // If the currency has conversion rates (could be the case that these still have to get their first update)
                if ($currency->conversion_rates != null and count($currency->conversion_rates) > 0)
                {
                    // Loop through all of the currency's conversion rates
                    foreach ($currency->conversion_rates as $code => $conversion_rate)
                    {
                        $currencyRates["rates"][] = [
                            "text" => $currency->code." - ".$code,
                            "rate" => $conversion_rate
                        ];
                    }
                }

                $out[] = $currencyRates;
            }
        }

        // Return the output as a Collection
        return collect($out);
    }

    /**
     * Convert
     * Convert x amount of currency a to y amount of currency b
     * 
     * @param       App\Models\Currency         The currency we are converting FROM
     * @param       App\Models\Currency         The currency we are converting TO
     * @param       float                       The amount we are converting
     * @return      float                       The converted amount
     */
    public function convert(Currency $baseCurrency, Currency $targetCurrency, $amount)
    {
        // Determine the conversion rate
        $conversionRate = null;
        foreach ($baseCurrency->conversion_rates as $code => $rate)
        {
            if ($code == $targetCurrency->code)
            {
                $conversionRate = $rate;
            }
        }

        // Convert te price using the conversion rate we just found
        $convertedPrice = $amount * $conversionRate;
        
        // Return the float value rounded to 2 decimals
        return floatval(number_format($convertedPrice, 2, ".", ""));
    }
}