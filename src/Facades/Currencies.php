<?php

namespace Hermes\Currencies\Facades;

use Illuminate\Support\Facades\Facade;

class Currencies extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "currencies";
    }
}