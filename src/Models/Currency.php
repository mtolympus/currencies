<?php

namespace Hermes\Currencies\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = "currencies";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = [
        "code",
        "name",
        "symbol",
        "html_code",
        "html_entity",
        "conversion_rates"
    ];
    protected $casts = [
        "conversion_rates" => "array"
    ];
}