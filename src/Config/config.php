<?php

/**
 * Currencies Package Configuration
 * 
 * Below you will find all the settings regarding the currencies package
 * 
 * @author          Nick Verheijen <verheijen.webdevelopment@gmail.com>
 * @version         1.0.0
 */

return [

    /**
     * 3rd party API service #1: Fixer.io
     * https://fixer.io/
     */

    // Fixer.io API key
    'fixer_api_key' => env('FIXER_API_KEY'),
    
    // Do we have premium access with our API key?
    'fixer_premium' => env('FIXER_PREMIUM', false),

];