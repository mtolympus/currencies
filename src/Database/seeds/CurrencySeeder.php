<?php

use Hermes\Currencies\Models\Currency;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return      void
     */
    public function run()
    {
        $this->emptyDatabase();
        $this->createCurrencies();
    }

    /**
     * Empty database
     * 
     * @return      void
     */
    private function emptyDatabase()
    {
        DB::table("currencies")->delete();
    }

    /**
     * Create currencies
     * 
     * @return      void
     */
    private function createCurrencies()
    {
        // Euro
        $euro = Currency::create([
            "code" => "EUR",
            "name" => "Euro",
            "symbol" => "€",
            "html_code" => "&#8374;",
            "html_entity" => "&euro;",
        ]);

        // Dollar
        $dollar = Currency::create([
            "code" => "USD",
            "name" => "United States Dollar",
            "symbol" => "$",
            "html_code" => "&#36;",
            "html_entity" => "&dollar;"
        ]);

        // Pound
        $pound = Currency::create([
            "code" => "GBP",
            "name" => "Pound Sterling",
            "symbol" => "£",
            "html_code" => "&#163;",
            "html_entity" => "&pound;"
        ]);

        // Yen
        $yen = Currency::create([
            "code" => "JPY",
            "name" => "Japanese Yen",
            "symbol" => "¥",
            "html_code" => "&#165;",
            "html_entity" => "&yen;"
        ]);
    }
}