<?php

namespace Hermes\Currencies\Providers;

use Hermes\Currencies\Currencies;
use Illuminate\Support\ServiceProvider;

class CurrenciesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootstrapConfiguration();
        $this->bootstrapMigrations();
        $this->bootstrapSeeds();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the Currencies service
        $this->app->singleton("currencies", function() {
            return new Currencies;
        });
    }
    
    /**
     * Bootstrap configuration
     * Sets up merging & publishing of the config file
     * 
     * @return      void
     */
    private function bootstrapConfiguration()
    {
        // Setup merging of the config file 
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "currencies");

        // Setup publishing of the config file
        $this->publishes([__DIR__."/../Config/config.php" => config_path("currencies.php")]);
    }

    /**
     * Bootstrap migrations
     * 
     * @return      void
     */
    private function bootstrapMigrations()
    {
        // Setup migration loading
        $this->loadMigrationsFrom(__DIR__."/../Database/migrations");

        // Setup migration publishing in case modifications to the database structure need to be made.
        $this->publishes([__DIR__."/../Database/migrations" => database_path("migrations")], "database");
    }

    /**
     * Bootstrap seeds
     * 
     * @return      void
     */
    private function bootstrapSeeds()
    {
        // Setup seed publishing
        $this->publishes([__DIR__."/../Database/seeds" => database_path("seeds")], "database");
    }
}   